How to Deploy

Step 0: login to mysql db and run below script:

CREATE TABLE `realm` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `realm_key` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_l5mqf3nyp9acp293si86e5o2g` (`name`)
);

Step 1 : clone this project "git clone https://test852abcd123@bitbucket.org/test852abcd123/brighttalk.git"

Step 2 : change your msyql db config in /src/resource/application.yml
  spring.datasource.url: jdbc:mysql://localhost:3306/{database}
  spring.datasource.username: {loginName}
  spring.datasource.password: {password}

Step 3 : browse into the top directory of this project and then run "mvn clean install"

Step 4 : browse into target file of this project and get the war file "brightTalk-0.0.1-SNAPSHOT.war"
         (suggest to rename it "brightTalk.war")

Step 5 : put "brightTalk.war" into tomcat/webapps/
         and then browse into tomcat/bin and run "./start.sh".

Step 6 :
Get url  : http://{host}:{port}/brightTalk/service/user/realm/{id}
Post url : http://{host}:{port}/brightTalk/service/user/realm

Summary of Work:

Mainly using spring-boot and spring mvc to do the developement.
First, i try to draw the realm entity and link it up with db through hibernate and repository.
Second, i design the rest controller part to handle request and response.
And then,i link the controller and the entity together through realmService to finish the development.
Also, i use junit and Mockito to do the testing part(test coverage as below).

Unit test case for realmRepository and the realmService
SIT case:
  create Realm By Json and Xml request(both includeing successful,without name and duplicated name case)
  get Realm include both json/xml response(both including successful,invalid id and not found case)




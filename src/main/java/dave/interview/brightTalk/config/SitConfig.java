package dave.interview.brightTalk.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan(value = "dave.interview.brightTalk")
@EnableJpaRepositories(value = "dave.interview.brightTalk")
public class SitConfig {

}

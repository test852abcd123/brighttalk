package dave.interview.brightTalk.Repository;

import dave.interview.brightTalk.entity.Realm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealmRepository extends CrudRepository<Realm, Long> {
    boolean existsByName(String name);
}

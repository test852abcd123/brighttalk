package dave.interview.brightTalk.controller;

import dave.interview.brightTalk.entity.Realm;
import dave.interview.brightTalk.obj.Error;
import dave.interview.brightTalk.service.RealmService;
import org.hibernate.validator.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@RestController
@RequestMapping(value = "/service/user/realm")
public class RealmController {

    @Autowired
    private RealmService realmService;

    @PostMapping(headers={"content-type=application/json;charset=UTF-8,application/xml;charset=UTF-8"})
    ResponseEntity newRealm(@RequestHeader("content-type") String contentType,@RequestBody Realm realm) {
        //Check realmName is not null
        if(StringHelper.isNullOrEmptyString(realm.getName())){
            return genResponse(new Error(Error.INVALID_REALM_NAME),HttpHeaders.CONTENT_TYPE, contentType,
                    HttpStatus.BAD_REQUEST);
        }
        //Check realmName not exist before
        if(realmService.checkIsExistByName(realm.getName())) {
            return genResponse(new Error(Error.DUPLICATE_REALM_NAME),HttpHeaders.CONTENT_TYPE, contentType,
                    HttpStatus.BAD_REQUEST);
        }
        return genResponse(realmService.createRealm(realm),HttpHeaders.CONTENT_TYPE, contentType,HttpStatus.CREATED);
    }


    @GetMapping(value = "/{id}",produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE  })
    ResponseEntity getRealm(@RequestHeader("Accept") String accept,@PathVariable Long id){
        Optional<Realm> realm = realmService.getRealm(id);
        //Check is Exist or not
        if(!realm.isPresent())
            return genResponse(new Error(Error.REAL_NOT_FOUND),HttpHeaders.ACCEPT, accept, HttpStatus.NOT_FOUND);
        return genResponse(realm.get(), HttpHeaders.ACCEPT, accept,HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity handleTypeMismatch(MethodArgumentTypeMismatchException ex) {
        return  genResponse(new Error(Error.INVALID_ARGUMENT),null,null,HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity genResponse(Object body,String header,String headerParam, HttpStatus status){
        return new ResponseEntity(
                body,
                header!=null?new HttpHeaders() {{
                    put(header, new ArrayList<String>(Arrays.asList(headerParam)));
                }}:null,
                status);
    }
}

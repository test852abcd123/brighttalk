package dave.interview.brightTalk.service;

import dave.interview.brightTalk.Repository.RealmRepository;
import dave.interview.brightTalk.entity.Realm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RealmServiceImpl implements  RealmService{

    @Autowired
    private RealmRepository realmRepository;

    @Override
    public Realm createRealm(Realm realm) {
        //set key
        realm.setKey("testKey");
        //save to db
        return realmRepository.save(realm);
    }

    @Override
    public boolean checkIsExistByName(String name) {
        return realmRepository.existsByName(name);
    }

    @Override
    public Optional<Realm> getRealm(Long id) {
        return realmRepository.findById(id);
    }
}

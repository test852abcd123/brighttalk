package dave.interview.brightTalk.service;

import dave.interview.brightTalk.entity.Realm;

import java.util.Optional;

public interface RealmService {
    Realm createRealm(Realm realm);
    boolean checkIsExistByName(String name);
    Optional<Realm> getRealm(Long id);
}

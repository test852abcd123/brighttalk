package dave.interview.brightTalk.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Realm")
public class Realm implements Serializable {

    public Realm() { }
    public Realm(String name, String description, String key) {
        this.name = name;
        this.description = description;
        this.key = key;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private Long id;

    @Column(name="NAME",unique = true)
    private String name;

    @Column(name="DESCRIPTION",length = 255)
    private String description;

    @Column(name="REALM_KEY",length = 32)
    private String key;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Realm)) return false;

        Realm realm = (Realm) o;

        if (id != null ? !id.equals(realm.id) : realm.id != null) return false;
        if (name != null ? !name.equals(realm.name) : realm.name != null) return false;
        if (description != null ? !description.equals(realm.description) : realm.description != null) return false;
        return key != null ? key.equals(realm.key) : realm.key == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }
}

package dave.interview.brightTalk.obj;

public class Error {

    public static final String DUPLICATE_REALM_NAME="DuplicateRealmName";
    public static final String INVALID_REALM_NAME="InvalidRealmName";
    public static final String INVALID_ARGUMENT="InvalidArgument";
    public static final String REAL_NOT_FOUND="RealmNotFound";

    private String code;

    public Error(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

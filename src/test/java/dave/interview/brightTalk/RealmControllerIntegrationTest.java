package dave.interview.brightTalk;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import dave.interview.brightTalk.config.SitConfig;
import dave.interview.brightTalk.controller.RealmController;
import dave.interview.brightTalk.entity.Realm;
import dave.interview.brightTalk.obj.Error;
import dave.interview.brightTalk.service.RealmService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(
        webEnvironment= SpringBootTest.WebEnvironment.MOCK,
        classes = RealmController.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource(
        locations = "classpath:application-sit.yml")
@EnableWebMvc
@ContextConfiguration(classes= SitConfig.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RealmControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RealmService realmService;

    String url ="/service/user/realm";

    @Test
    public void createRealmJson()
            throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String invalidRealmError = mapper.writeValueAsString(new Error(Error.INVALID_REALM_NAME));
        String duplicateError = mapper.writeValueAsString(new Error(Error.DUPLICATE_REALM_NAME));

        //Case 400 invalid
        Realm realm = new Realm(null,"desc",null);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));

        String requestJson=mapper.writeValueAsString(realm);
        MockHttpServletRequestBuilder request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_JSON_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().json(invalidRealmError))
        .andReturn();

        //Case 201 success
        realm.setName("test1");

        requestJson = mapper.writeValueAsString(realm);
        request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_JSON_UTF8);
        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(realm.getName()))
                .andExpect(jsonPath("$.description").value(realm.getDescription()))
                .andExpect(jsonPath("$.key").value("testKey"))
                .andReturn();

        //Case 400 is duplicate
        requestJson = mapper.writeValueAsString(realm);
        request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_JSON_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().json(duplicateError))
                .andReturn();
    }

    @Test
    public void createRealmXml()
            throws Exception {
        XmlMapper mapper = new XmlMapper();
        String invalidRealmError = mapper.writeValueAsString(new Error(Error.INVALID_REALM_NAME));
        String duplicateError = mapper.writeValueAsString(new Error(Error.DUPLICATE_REALM_NAME));

        //Case 400 invalid
        Realm realm = new Realm(null,"desc",null);
        MediaType MEDIA_TYPE_XML_UTF8 = new MediaType("application", "xml", java.nio.charset.Charset.forName("UTF-8"));

        String requestJson=mapper.writeValueAsString(realm);
        MockHttpServletRequestBuilder request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_XML_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().xml(invalidRealmError))
                .andReturn();

        //Case 201 success
        realm.setName("test2");

        requestJson = mapper.writeValueAsString(realm);
        request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_XML_UTF8);
        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(xpath("Realm/name").string(realm.getName()))
                .andExpect(xpath("Realm/description").string(realm.getDescription()))
                .andExpect(xpath("Realm/key").string("testKey"))
                .andReturn();

        //Case 400 is duplicate
        requestJson = mapper.writeValueAsString(realm);
        request = post(url);
        request.content(requestJson);
        request.contentType(MEDIA_TYPE_XML_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().xml(duplicateError))
                .andReturn();
    }

    @Test
    public void getRealmJson()
            throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String notFoundError = mapper.writeValueAsString(new Error(Error.REAL_NOT_FOUND));
        String invalidError = mapper.writeValueAsString(new Error(Error.INVALID_ARGUMENT));
        //Case 200 success
        Realm realm = new Realm("test3","desc",null);
        realmService.createRealm(realm);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
        MockHttpServletRequestBuilder request = get(url+"/3");
        request.accept(MEDIA_TYPE_JSON_UTF8);
        MvcResult result= mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(realm.getName()))
                .andExpect(jsonPath("$.description").value(realm.getDescription()))
                .andExpect(jsonPath("$.key").value("testKey"))
                .andReturn();
        //Case 404
        request = get(url+"/999");
        request.accept(MEDIA_TYPE_JSON_UTF8);
        mvc.perform(request)
                .andExpect(status().isNotFound())
                .andExpect(content().json(notFoundError))
                .andReturn();
        //Case 400
        request = get(url+"/abc");
        request.accept(MEDIA_TYPE_JSON_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().json(invalidError))
                .andReturn();


    }


    @Test
    public void getRealmXml()
            throws Exception {
        XmlMapper mapper = new XmlMapper();
        String notFoundError = mapper.writeValueAsString(new Error(Error.REAL_NOT_FOUND));
        String invalidError = mapper.writeValueAsString(new Error(Error.INVALID_ARGUMENT));
        //Case 200 success
        Realm realm = new Realm("test4","desc",null);
        realmService.createRealm(realm);
        MediaType MEDIA_TYPE_XML_UTF8 = new MediaType("application", "xml", java.nio.charset.Charset.forName("UTF-8"));
        MockHttpServletRequestBuilder request = get(url+"/4");
        request.accept(MEDIA_TYPE_XML_UTF8);
        MvcResult result= mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(xpath("Realm/name").string(realm.getName()))
                .andExpect(xpath("Realm/description").string(realm.getDescription()))
                .andExpect(xpath("Realm/key").string("testKey"))
                .andReturn();
        //Case 404
        request = get(url+"/999");
        request.accept(MEDIA_TYPE_XML_UTF8);
        mvc.perform(request)
                .andExpect(status().isNotFound())
                .andExpect(content().xml(notFoundError))
                .andReturn();
        //Case 400
        request = get(url+"/abc");
        request.accept(MEDIA_TYPE_XML_UTF8);
        mvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(content().xml(invalidError))
                .andReturn();


    }
}

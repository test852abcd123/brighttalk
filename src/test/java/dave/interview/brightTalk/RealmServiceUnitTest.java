package dave.interview.brightTalk;

import dave.interview.brightTalk.Repository.RealmRepository;
import dave.interview.brightTalk.entity.Realm;
import dave.interview.brightTalk.service.RealmService;
import dave.interview.brightTalk.service.RealmServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.ArgumentMatchers.any;

import java.util.Optional;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
public class RealmServiceUnitTest {

    @TestConfiguration
    static class RealmServiceImplConfig {
        @Bean
        public RealmService realmService() {

            return new RealmServiceImpl();
        }
    }
    @Autowired
    private RealmService realmService;

    @MockBean
    private RealmRepository realmRepository;

    @Before
    public void setUp() {
        Realm realm = new Realm("name","desc","testKey");

        Mockito.when(realmRepository.existsByName(realm.getName()))
                .thenReturn(true);
        Mockito.when(realmRepository.findById(1L))
                .thenReturn(Optional.of(realm));
        Mockito.when(realmRepository.save(realm))
                .thenReturn(realm);
    }

    @Test
    public void realmIsExist() {
        String name = "name";
        Boolean result = realmService.checkIsExistByName(name);

        assertEquals(result,Boolean.TRUE);
    }
    @Test
    public void getRealm() {
        Long id = 1L;
        String name = "name";
        Optional<Realm> result = realmService.getRealm(id);

        assertEquals(result.get().getName(),name);
    }
    //TODO
    @Test
    public void createRealm() {
        Realm realm = new Realm();
        realm.setName("name");
        realm.setDescription("desc");
        Realm result = realmService.createRealm(realm);
        assertEquals(result.getKey(),"testKey");
    }

}

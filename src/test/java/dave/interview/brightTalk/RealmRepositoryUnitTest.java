package dave.interview.brightTalk;

import dave.interview.brightTalk.Repository.RealmRepository;
import dave.interview.brightTalk.entity.Realm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RealmRepositoryUnitTest {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RealmRepository realmRepository;

    @Before
    public void beforeTest() {
        System.out.println("Before");
    }

    @After
    public void afterTest() {
        System.out.println("After");
    }
    @Test
    public void checkIsExist() {
        // given
        Realm realm = new Realm("name","desc","key");
        entityManager.persist(realm);
        entityManager.flush();

        // when
        Boolean result = realmRepository.existsByName(realm.getName());

        // then
        assertEquals(result,Boolean.TRUE);
    }

}
